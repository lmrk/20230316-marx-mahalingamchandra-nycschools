package com.nyc.schools.jpmc.ui.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.nyc.schools.jpmc.model.NYCSchoolModel
import com.nyc.schools.jpmc.model.NYCSchoolSATModel
import com.nyc.schools.jpmc.repository.Repository
import kotlinx.coroutines.*

class MainViewModel : ViewModel() {

    val schoolData = MutableLiveData<List<NYCSchoolModel>>()
    private val satData = HashMap<String, NYCSchoolSATModel>()

    private val _schoolData: LiveData<List<NYCSchoolModel>> get() = schoolData

    fun getSchoolData(): LiveData<List<NYCSchoolModel>> {
        return _schoolData
    }

    fun getSchoolSATPairValue(): HashMap<String, NYCSchoolSATModel> {
        return satData
    }

    fun loadData() {
        viewModelScope.launch(Dispatchers.IO) {

            val schoolListResponse = async {
                Repository.fetchSchoolsData()
            }.await()

            val schoolSATListResponse = async {
                Repository.fetchSchoolsSATData()
            }.await()

            delay(2000)
            withContext(Dispatchers.Main) {
                for (satElement in schoolSATListResponse) {
                    satData.put(satElement.dbn, satElement)
                }
                schoolData.postValue(schoolListResponse)
            }
        }
    }

    override fun onCleared() {
        super.onCleared()
        satData.clear()
    }
}
