package com.nyc.schools.jpmc.model

data class NYCSchoolModel(
    val dbn: String,
    val school_name: String,
    val building_code: String,
    val location: String,
    val phone_number: String,
    val fax_number: String,
    val school_email: String,
    val city: String,
    val state_code: String,
    val website: String
)