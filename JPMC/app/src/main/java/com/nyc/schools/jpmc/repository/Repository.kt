package com.nyc.schools.jpmc.repository

import com.google.gson.Gson
import com.google.gson.JsonArray
import com.nyc.schools.jpmc.model.NYCSchoolModel
import com.nyc.schools.jpmc.model.NYCSchoolSATModel
import okhttp3.*
import java.io.IOException

class Repository {

    companion object {
        private val BASE_URL = "https://data.cityofnewyork.us/"
        private val NYC_SCHOOL_URL = BASE_URL.plus("resource/s3k6-pzi2.json")
        private val NYC_SCHOOL_SAT_URL = BASE_URL.plus("resource/f9bf-2cp4.json")

        fun fetchSchoolsData(): List<NYCSchoolModel> {

            var nycSchoolList = mutableListOf<NYCSchoolModel>()
            val client = OkHttpClient().newBuilder()
                .build()
            val request: Request = Request.Builder()
                .url(NYC_SCHOOL_URL)
                .method("GET", null)
                .build()
            try {
                client.newCall(request).enqueue(object : Callback {
                    override fun onFailure(call: Call, e: IOException) {
                        e.printStackTrace()
                    }

                    override fun onResponse(call: Call, response: Response) {

                        val jsonData: String? = response.body()?.string()
                        val gson = Gson()
                        val array: JsonArray = gson.fromJson(jsonData, JsonArray::class.java)
                        for (element in array) {
                            val nycSchoolModel = gson.fromJson(element, NYCSchoolModel::class.java)
                            nycSchoolList.add(nycSchoolModel)
                        }
                    }
                })
            } catch (e: Exception) {
                e.printStackTrace()
            }

            return nycSchoolList
        }

        fun fetchSchoolsSATData(): List<NYCSchoolSATModel> {

            var nycSchoolList = mutableListOf<NYCSchoolSATModel>()
            val client = OkHttpClient().newBuilder()
                .build()
            val request: Request = Request.Builder()
                .url(NYC_SCHOOL_SAT_URL)
                .method("GET", null)
                .build()
            try {
                client.newCall(request).enqueue(object : Callback {
                    override fun onFailure(call: Call, e: IOException) {
                        e.printStackTrace()
                    }

                    override fun onResponse(call: Call, response: Response) {
                        val jsonData: String? = response.body()?.string()
                        val gson = Gson()
                        val array: JsonArray = gson.fromJson(jsonData, JsonArray::class.java)
                        for (element in array) {
                            val nycSchoolModel =
                                gson.fromJson(element, NYCSchoolSATModel::class.java)
                            nycSchoolList.add(nycSchoolModel)
                        }
                    }
                })
            } catch (e: Exception) {
                e.printStackTrace()
            }
            return nycSchoolList
        }
    }
}