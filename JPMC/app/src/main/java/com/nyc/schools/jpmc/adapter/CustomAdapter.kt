package com.nyc.schools.jpmc.adapter

import android.app.Activity
import android.app.Dialog
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.Button
import android.widget.LinearLayout
import android.widget.TextView
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import com.nyc.schools.jpmc.R
import com.nyc.schools.jpmc.model.NYCSchoolModel
import com.nyc.schools.jpmc.model.NYCSchoolSATModel

class CustomAdapter(
    private val activity: Activity,
    private val mList: List<NYCSchoolModel>,
    private val satData: HashMap<String, NYCSchoolSATModel>
) :
    RecyclerView.Adapter<CustomAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.card_view_design, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val itemsViewModel = mList[position]
        holder.schoolName.text = itemsViewModel.school_name
        holder.city.text = itemsViewModel.city
        holder.phoneNumber.text = itemsViewModel.phone_number
        holder.email.text = itemsViewModel.school_email
        holder.entireView.setOnClickListener {
            val sat = satData[itemsViewModel.dbn]
            sat?.let {
                if (sat.dbn.isNotEmpty()) {
                    showSATInfo(activity, sat)
                } else {
                    Toast.makeText(activity, "Thisd da", Toast.LENGTH_LONG).show()
                }
            }
        }
    }

    override fun getItemCount(): Int {
        return mList.size
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val schoolName: TextView = itemView.findViewById(R.id.school_name)
        val city: TextView = itemView.findViewById(R.id.city)
        val phoneNumber: TextView = itemView.findViewById(R.id.phone_number)
        val email: TextView = itemView.findViewById(R.id.email)
        val entireView: LinearLayout = itemView.findViewById(R.id.entire_view)
    }

    private fun showSATInfo(activity: Activity, data: NYCSchoolSATModel) {
        val dialog = Dialog(activity)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setCancelable(true)
        dialog.setContentView(R.layout.dialog_custom_layout)

        val schoolName = dialog.findViewById(R.id.schoolname) as TextView
        schoolName.text = data.school_name

        val mathScore = dialog.findViewById(R.id.math_score) as TextView
        mathScore.text = activity.getString(R.string.maths_score)
            .plus(data.sat_math_avg_score)

        val readingScore = dialog.findViewById(R.id.reading_score) as TextView
        readingScore.text = activity.getString(R.string.reading_score)
            .plus(data.sat_critical_reading_avg_score)

        val writingScore = dialog.findViewById(R.id.writing_score) as TextView
        writingScore.text = activity.getString(R.string.writing_score)
            .plus(data.sat_writing_avg_score)

        val ok = dialog.findViewById(R.id.ok) as Button
        ok.setOnClickListener { dialog.cancel() }

        dialog.show()
    }
}
