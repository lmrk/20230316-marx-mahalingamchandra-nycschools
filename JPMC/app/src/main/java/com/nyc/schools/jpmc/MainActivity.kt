package com.nyc.schools.jpmc

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.nyc.schools.jpmc.adapter.CustomAdapter
import com.nyc.schools.jpmc.ui.viewmodel.MainViewModel

class MainActivity : AppCompatActivity() {

    private lateinit var viewModel: MainViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val recyclerview = findViewById<RecyclerView>(R.id.recyclerview)
        recyclerview.layoutManager = LinearLayoutManager(this)

        viewModel = ViewModelProvider(this)[MainViewModel::class.java]

        viewModel.schoolData.observe(this) {
            val parameterSchool =
                viewModel.getSchoolData().value?.let { it1 ->
                    CustomAdapter(this,
                        it1,
                        viewModel.getSchoolSATPairValue()
                    )
                }
            recyclerview.adapter = parameterSchool
        }

        viewModel.loadData()
    }
}